// Activity No. 2
db.fruits.aggregate([
    {
      $match: {
        onSale: true,
      }
    },
    {
      $count: "onSale"
    }
])
// Activity No. 2 END

// Activity No. 3
db.fruits.aggregate([
    {
      $match: {
        stock: {$gte:20},
      }
    },
    {
      $count: "stock"
    }
])
// Activity No. 3 END

// Activity No. 4
db.fruits.aggregate([
	{
		$match: {onSale: true}
	},
	{
		$group: {
			_id: "$supplier_id",
			avg_price: {$avg: "$price"}
		}
	}
])
// Activity No. 4 END

// Activity No. 5
db.fruits.aggregate([
	{
		$group: {
			_id: "$supplier_id",
			max_price: {$max: "$price"}
		}
	}
])
// Activity No. 5 END

// Activity No. 6
db.fruits.aggregate([
	{
		$group: {
			_id: "$supplier_id",
			min_price: {$min: "$price"}
		}
	}
])
// Activity No. 6 END